package com.example.demokit;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        final TextView tv1 = findViewById(R.id.tv_1);
        final TextView tv2 = findViewById(R.id.tv_2);
        final TextView tv3 = findViewById(R.id.tv_3);
        Button logout = findViewById(R.id.btn_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AccountKit.logOut();
                if (isLogout()) finish();
            }
        });
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get Account Kit ID
                String accountKitId = account.getId();
                tv1.setText("ID: " + accountKitId);
                // Get phone number
                if (account.getPhoneNumber() != null)
                    tv2.setText("phone number: " + account.getPhoneNumber().toString());
                // Get email
                if (account.getEmail() != null)
                    tv3.setText("email: " + account.getEmail());
            }

            @Override
            public void onError(final AccountKitError error) {

            }
        });
    }

    private boolean isLogout() {
        AccessToken accessToken = AccountKit.getCurrentAccessToken();
        return (accessToken != null) ? false : true;
    }
}
